package com.harunoguzorhan.q5;

public class UpdateOperation implements Operation {

	private ParameterDTO dto;
	private boolean isOperationMade = false;

	public UpdateOperation(ParameterDTO dto) {
		this.dto = dto;
	}

	@Override
	public void doControl() {
		if (isOperationMade) {
			System.out.println("Operation is made");
		} else {
			System.out.println("Operation isn't made");
		}
	}

	@Override
	public void makeOperation() {
		Logger.log("update operation is invoked for: " + dto);
		OperationFactory.persistenceImpl.update(dto);
		Logger.log("operation is successfully completed");
		isOperationMade = true;

	}

}
