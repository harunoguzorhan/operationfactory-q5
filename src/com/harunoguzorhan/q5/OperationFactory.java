package com.harunoguzorhan.q5;

public class OperationFactory {

	public static final PersistenceImpl persistenceImpl = new PersistenceImpl();

	public static Operation getInstance(ParameterDTO dto) {

		if (dto.getState().equals(State.INSERT)) {
			return new InsertOperation(dto);
		} else if (dto.getState().equals(State.UPDATE)) {
			return new UpdateOperation(dto);
		} else if (dto.getState().equals(State.DELETE)) {
			return new DeleteOperation(dto);
		}

		return null;
	}

}
