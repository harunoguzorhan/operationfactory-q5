package com.harunoguzorhan.q5;

import java.util.HashMap;
import java.util.Map;

public class PersistenceImpl {
	Map<Long, ParameterDTO> parameters = new HashMap<Long, ParameterDTO>();

	public void insert(ParameterDTO dto) {
		parameters.put(dto.getId(), dto);
	}

	public void update(ParameterDTO dto) {
		if(parameters.get(dto.getId())!=null){
		parameters.put(dto.getId(), dto);
		} else{
			//TODO nothing to update
		}
		
	}

	public void delete(ParameterDTO dto) {
		parameters.remove(dto.getId());
	}

	public ParameterDTO findById(long id) {
		return parameters.get(id);
	}
}
