package com.harunoguzorhan.q5;

public class Test {

	public static void main(String[] args) throws Exception {
		ParameterDTO dto = new ParameterDTO();
		dto.setState(State.DELETE);
		dto.setParameterKey("Key");
		dto.setParameterValue("Value");
		dto.setId(1L);
		Operation delete = OperationFactory.getInstance(dto);
		delete.makeOperation();
	}
}