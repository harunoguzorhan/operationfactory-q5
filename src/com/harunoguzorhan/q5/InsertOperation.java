package com.harunoguzorhan.q5;

public class InsertOperation implements Operation {
	
	private ParameterDTO dto;
	private boolean isOperationMade = false;
	
	public InsertOperation(ParameterDTO dto) {
		this.dto = dto;
	}

	@Override
	public void doControl() {
		if (isOperationMade) {
			System.out.println("Operation is made");
		} else {
			System.out.println("Operation isn't made");
		}
		
	}

	@Override
	public void makeOperation() {
		Logger.log("insert operation is invoked for: " + dto);
		OperationFactory.persistenceImpl.insert(dto);
		Logger.log("operation is successfully completed");
		isOperationMade = true;
	}

}
